#pragma once

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <math.h>
#include <functional>

template <class T>
class HT
{
private:
    std::function<int(T)> hashLambda;//= [](T elem)->int{return 0;};
    // Создать псевдоним для хранимых данных using
    std::vector<std::list<T>> table;
    using container = decltype(table);
    unsigned numOfElems;
    int getHash(const T &elem) const;
    int getHash(int elem)  const;
    int	getHash(double elem) const;
public:
     HT();
    ~HT();
    explicit HT(int hashSize);
    void setHashFunction(std::function<int(T)> hashFoo);
    void normalize();
    void addElement(const T& elem);
    void resize(int size);
    void del(const T& elem);
    bool is(const T& elem);
    std::list<T> getListOfElements() const;
    std::list<T>* operator [](int hash);
    int size() const;
};

template<>
int HT<std::string>::getHash(const std::string &elem) const
{
    int h = 0;
    for(unsigned j=0; j<elem.size(); ++j)
    {
        h += elem[j];
    }
    return h%table.size();
}

template <class T>
int HT<T>::getHash(const T& elem) const
{
   return hashLambda(elem)%table.size();
}

template <class T>
int HT<T>::getHash(int elem) const
{
    return elem%table.size();
}

template <class T>
int HT<T>::getHash(double elem) const
{
    return (int)(elem * pow(10, ( (int)elem % 10 )) ) % table.size();
}

template <class T>
void HT<T>::setHashFunction(std::function<int(T)> hashFoo)
{
    hashLambda=hashFoo;
}

template <class T>
std::list<T>* HT<T>::operator [](int hash)
{
    return &table[hash];
}

template <class T>
HT<T>::HT(int hashSize)
{
    if(hashSize<=0)
    {
        hashSize=2;
    }
    table.resize(hashSize);
    numOfElems=0;
}

template <class T>
std::list<T> HT<T>::getListOfElements() const
{
    std::list<T> output;
    for(unsigned i=0 ; i<table.size() ; i++)
    {
        for(auto it = table[i].begin() ; it!=table[i].end() ; ++it)
        {
            output.push_back(*it);
        }
    }
    return output;
}

template <class T>
void HT<T>::normalize()
{
    table.resize(table.size()*2);
    container temp;
    temp.resize(table.size());
    for(unsigned i=0 ; i<table.size() ; i++)
    {
        for(auto it = table[i].begin() ; it!=table[i].end() ; ++it)
        {
            temp[getHash(*it)].push_back(*it);
        }
    }
    table=temp;
}

template <class T>
HT<T>::HT()
{
    numOfElems=0;
    table.resize(2);
}

template <class T>
HT<T>::~HT(){ }

template <class T>
void HT<T>::addElement(const T& elem)
{
    numOfElems+=1;
    if(numOfElems > table.size()/2 )
        normalize();

    int hash = getHash(elem);

    if(table[hash].empty())
    {
        table[hash].push_back(elem);
    }
    else
    {
        if(is(elem))
        {
            return;
        }
        else
        {
            table[hash].push_back(elem);
        }
    }
}

template <class T>
void HT<T>::resize(int size)
{
    table.resize(size);
    normalize();
}

template <class T>
void HT<T>::del(const T& elem)
{
    int hash = getHash(elem);

    for(auto it = table[hash].begin(); it!= table[hash].end(); ++it)
    {
        if(*it == elem)
        {
            table[hash].erase(it);
            return;
        }
    }
}

template <class T>
bool HT<T>::is(const T& elem)
{
    int hash= getHash(elem);
    for(auto it = table[hash].begin() ; it!= table[hash].end() ; ++it)
    {
        if(*it == elem)
        {
            return true;
        }
    }
    return false;
}

template <class T>
int HT<T>::size() const
{
    return table.size();
}
