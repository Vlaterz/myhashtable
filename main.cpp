#include <iostream>
#include <unordered_set>

#include "ht.hpp"

using namespace std;

struct A //must have overload for op"=="
{
    int num;
    A(int i)
    {
        num=i;
    }
    bool operator ==(A compare)
    {
        if(compare.num == this->num)
        {
            return true;
        }
        return false;
    }
};

int main()
{
    HT<A> ht1;
    ht1.setHashFunction([](A elem)->int{return elem.num;} );

    HT<string> ht;

    ht1.addElement(A(7));
    ht1.addElement(A(14));
    ht1.addElement(A(3));

    ht.addElement("Some string4");
    ht.addElement("Some string1");
    ht.addElement("Some string3");

    cout<<"Full list of HT<string>"<<endl;
    for(auto& li: ht.getListOfElements())
    {
      std::cout << li << std::endl;
    }
    cout<<endl;
    cout<<"Full list of HT<A>"<<endl;
    for(auto& li: ht1.getListOfElements())
    {
      std::cout << li.num << std::endl;
    }
    cout<<endl;

    ht.del("Some string4");
    cout<<"New list of HT<string>"<<endl;
    for(auto& li: ht.getListOfElements())
    {
      std::cout << li << std::endl;
    }
    cout<<endl;

    cout<<"New list of HT<A>"<<endl;
    ht1.del(A(7));
    for(auto& li: ht1.getListOfElements())
    {
      std::cout << li.num << std::endl;
    }
    cout<<endl;

    for(int i=0 ; i<ht.size() ; i++)
    {
        for( list<string>::iterator it = ht[i]->begin() ; it!=ht[i]->end() ; it++)
        {
            cout<<i<<" "<<*it<<endl;
        }
    }

//---------------------------------------------------------------------------------------------
    HT<string>test;
    for(int i = 0 ; i<100000 ; i++)
    {
        //cout<<i<<endl;
        string temp;
        for(int i=0;i<rand()%24 ; i++)
        {
            temp.push_back(rand()%22+97);
        }
        test.addElement(temp);
    }

    for(int i=0 ; i<test.size() ; i++)
    {
        for( list<string>::iterator it = test[i]->begin() ; it!=test[i]->end() ; it++)
        {
            cout<<i<<" "<<*it<<endl;
        }
    }
   cout<<"sizeOf HT<string>: "<<test.size()<<endl;
    // Тест на колизии
    // 0..24, 1 000 000.
    cout<<"end of prog"<<endl;
    return 0;
}
